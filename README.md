# MVP_remote_bibliography

Minimal test to use a remotely sourced bibliography from LaTeX

---

This repository contains the 'remote bibliography', as well as a minimum viable product mvp.tex file to demonstrate it working (note that the .bib file can be deleted locally before compilation to demonstrate that it loads from the remote source)

### Usage:

To test using the remote bibliography - clone this repository, delete the .bib file, and ensure the following packages are installed: biblatex & biber (both should be available as unix commands if installed)

Then, run (in order) these commands

* latex mvp.tex; # will try to compile the .tex file (will complain about missing .bib)

* biber mvp; # will try to find the .bib it complained about missing, using the remote url

* pdflatex mvp.tex; # will re-compile with the newly downloaded bibliography, and generate a pdf file of the document
